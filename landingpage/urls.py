from django.urls import path
from . import views


app_name = 'landingpage'

urlpatterns = [
    path('', views.index, name='index'),
    path('FormLangganan',views.langganan_create_view, name= 'langganan_create'),
    path('FormLanggananBerhasil',views.langganan_berhasil, name= 'langganan_berhasil'),
    path('Berita',views.berita, name= 'berita'),
]
