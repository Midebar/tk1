from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest, response

from .models import Langganan
# Create your tests here.

class Landing_Page(TestCase):
   
    def index_is_exist (self) :
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def langgganan_create_is_exist (self) :
        response = Client().get('/FormLangganan')
        self.assertEqual(response.status_code, 200)

    def berita_create_is_exist (self) :
        response = Client().get('/Berita')
        self.assertEqual(response.status_code, 200)

    def langgganan_berhasil_is_exist (self) :
        response = Client().get('/FormLanggananBerhasil')
        self.assertEqual(response.status_code, 200)
    
    #view-test
    def tombol_submit_is_exist(self):
        response = Client().get('FormLangganan')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Submit", html_kembalian)

    def tombol_back_is_exist(self):
        response = Client().get('FormLangganan')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Back", html_kembalian)

     #model-test
    def test_models(self):
        Langganan.objects.create(namaOrang='test' , email='test@gmail.com' , noTelepon ='012345')
        counting = Langganan.objects.all().count()
        self.assertAlmostEquals(counting, 1)



