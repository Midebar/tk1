from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404

from .forms import LanggananForm
from .models import Langganan


# Create your views here.

def index(request):
    return render(request, 'home/index.html')

def berita(request):
    return render(request, 'home/berita.html')

def langganan_berhasil(request):
    count= Langganan.objects.all().count()
    context= {'count': count}
    return render(request, 'home/langganan_berhasil.html',context)

def langganan_create_view(request):
    form = LanggananForm()
    if request.method == 'POST':
        form = LanggananForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/FormLanggananBerhasil')
    return render(request, 'home/langganan_create.html', {'form':form})
