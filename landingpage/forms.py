from django import forms

from .models import Langganan

class LanggananForm(forms.ModelForm):
    namaOrang  = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Nama Kamu'}) , label = ' Nama = ' )
    email      = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Email Kamu'}) , label = ' Alamat Email = ')
    noTelepon  = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Nomor Telepon Kamu'}) , label = ' No Telepon = ')
  
    class Meta:
        model = Langganan
        fields = '__all__'
