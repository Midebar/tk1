from django.db import models

# Create your models here.
class Langganan(models.Model):
    namaOrang  = models.CharField(max_length = 30)
    email      = models.CharField(max_length = 30)
    noTelepon  = models.CharField(max_length = 30)

    def __str__(self):
        return self.namaOrang
