from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest, response
from .models import Search

# Create your tests here.
class StatistikCorona (TestCase):
    def test1_landingPage(self):
        response = Client().get('/statistik/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,'landingPage.html')
    
    def test2_result(self):
        response = Client().get('/statistik/result/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'result.html')
    
    def test3_searchModel(self):
        counter = Search.objects.all()
        Search.objects.create(search = 'DKI Jakarta')
        self.assertEqual(Search.objects.all().count(), 1)
    
    def test4_formTest(self):
        response = Client().get('/statistik/result/', data={'search': 'DKI Jakarta'})
        self.assertEqual(response.status_code, 200)

    
        
