from django.shortcuts import render, redirect
from .models import Search
from .forms import SearchForm
import requests

# Create your views here.
def search(request):
    form = SearchForm()
    return render(request, "landingPage.html", {'form': form})

def result(request):
    search = request.GET.get('search')
    response = requests.get('https://api.kawalcorona.com/indonesia/provinsi/').json()
    return render(request, 'result.html',{'response':response, 'search':search})

