from django.test import TestCase, Client
from django.apps import apps
from covidResearch.apps import CovidResearch
from covidResearch.models import News

class TestCovidResearch(TestCase):
    
    def test_url_home_exist(self):
        response = Client().get('/covidResearch/')
        self.assertEquals(response.status_code, 200)

    def test_url_upload_exist(self):
        response = Client().get('/covidResearch/uploadNews/')
        self.assertTemplateUsed(response, "upload_news.html")
    
    def test_url_search_exist(self):
        response = Client().get('/covidResearch/search/')
        self.assertEquals(response.status_code, 200)
    
    def test_app_name(self):
        self.assertEqual(CovidResearch.name, 'covidResearch')
        self.assertEqual(apps.get_app_config('covidResearch').name, 'covidResearch')
    
    def test_object_is_created_and_rendered(self):
        #News attr = title, author, desc, shortDesc. date_published and image is not required
        News.objects.create(title="Bentuk Virus Covid-19", author="Burhan",
        desc="Covid-19 merupakan virus dari famili coronavidae yang berbentuk seperti mahkota ", shortDesc="Penjelasan dan Sejarah Covid-19")
        response = Client().get('/covidResearch/')
        self.assertContains(response, "Covid")
        response = Client().get('/covidResearch/news/1')
        self.assertContains(response, "Burhan")