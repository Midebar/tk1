from django.urls import path
from covidResearch.views import home, uploadNews, detailNews, searchNews

app_name = "covidResearch"

urlpatterns = [
    path('', home, name="Home"),
    path('uploadNews/', uploadNews, name="Upload"),
    path('news/<int:pk>', detailNews, name="DetailNews"),
    path('search/', searchNews, name="Search")
]
