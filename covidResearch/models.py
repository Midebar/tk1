from django.db import models

# Create your models here.

def get_upload_path(instance, filename):
    model = instance.news.pk
    return f'{model}/images/{filename}'

class News(models.Model):
    title = models.CharField(max_length=30)
    author = models.CharField(max_length=50)
    date_published = models.DateTimeField(auto_now_add=True)
    desc = models.CharField(max_length=500)
    shortDesc = models.CharField(max_length=100)

class Image(models.Model):
    news = models.ForeignKey(News, default=None, related_name="image", on_delete=models.CASCADE)
    image = models.ImageField(upload_to=get_upload_path)
    default = models.BooleanField(default=False)






