from django import forms
from covidResearch.models import News, Image

class NewsForm(forms.ModelForm):
    title = forms.CharField(max_length=30, widget=forms.TextInput(attrs={'placeholder': 'Judul Artikel'}) , label = 'Judul' )
    author = forms.CharField(max_length=50, widget=forms.TextInput(attrs={'placeholder': 'Penulis Artikel'}) , label = 'Penulis' )
    desc = forms.CharField(max_length=500, widget=forms.Textarea(attrs={'placeholder': 'Isi artikel disini'}) , label = 'Isi Artikel' )
    shortDesc = forms.CharField(max_length=100, widget=forms.Textarea(attrs={'placeholder': 'Isian singkat artikel'}) , label = 'Deskripsi Artikel' )
    class Meta:
        model = News
        fields = ('title','author','desc','shortDesc',)

class ImageForm(forms.ModelForm):
    image = forms.ImageField(label = 'Gambar/Foto Kegiatan/Hasil Penelitian')
    class Meta:
        model = Image
        fields = ('image',)
