# TK1

Proyek TK1 - B09 - PPW - 2020

Anggota kelompok:
- Putu Wigunarta
- Kevin 
- Muhammad Ravi Shulthan Habibi
- Mikhael Deo Barli

Link herokuapp: https://heroku-tk1-b09-ppw.herokuapp.com/
Link Wireframe: https://www.figma.com/file/e6GWyYN2ZjduDoHXgNvnZk/TK-B09?node-id=0%3A1

Pipeline Status: passed

Fitur yang akan diimplementasikan:
- Landing Page
- Statistik Covid
- Covid as a Virus
- Dampak Covid dalam berbagai sektor

Deskripsi:
Kami membuat CovidResearch untuk mewadahi orang-orang umum untuk mengetahui tentang Covid dan mencegah masyarakat melihat hoax. Hal ini dikarenakan masih banyaknya masyarakat yang percaya dengan hoaks Covid-19 yang disebabkan karena ketidaktahuan mereka akan virus tersebut. Oleh karena itu, kami kelompok B09 berusaha membuat sebuah web yang isinya merupakan berita dan informasi yang berasal dari sumber terpercaya. Tentunya, Kami tidak hanya menyediakan informasi keadaan covid di Indonesia namun juga virus Covid itu sendiri.