from django.test import TestCase, Client, RequestFactory
from django.urls import reverse, resolve
from .models import DampakBerita

from http import HTTPStatus

from django.apps import apps
from dampak.apps import HomeConfig
from . import views
from . import models

class Tests(TestCase):
    def setUp(self):
        self.dampak = DampakBerita.objects.create(nama_berita='Test',nama_penulis='Budi',isi_berita='W')
        
    def test_list_url_is_resolved_welcome(self):
        response = Client().get('/dampak/')
        self.assertEquals(response.status_code, 200)

    def test_list_url_is_resolved_tambah(self):
        response = Client().get('/dampak/tambah/')
        self.assertEquals(response.status_code, 200)

    def test_list_url_is_resolved_lihatnondetail(self):
        response = Client().get('/dampak/lihatNonDetail/')
        self.assertEquals(response.status_code, 200)

    def test_list_url_is_resolved_lihat(self):
        response = Client().get('/dampak/lihat/')
        self.assertEquals(response.status_code, 200)
    
    def test_list_url_resolved_detail(self):
        response = Client().get('/dampak/detail_berita/1/')
        self.assertEquals(response.status_code, 200)
    
    def test_list_url_resolved_update(self):
        response = Client().get('/dampak/update_berita/1/')
        self.assertEquals(response.status_code, 200)
    
    def test_list_url_resolved_delete(self):
        response = Client().get('/dampak/delete_berita/1/')
        self.assertEquals(response.status_code, 200)

    def test_apps(self):
        self.assertEqual(HomeConfig.name, 'dampak')
        self.assertEqual(apps.get_app_config('dampak').name, 'dampak')

    def test_dampakberita(self):
        dampakberita = DampakBerita.objects.create(nama_berita='Dampak Berita Test')
        self.assertEqual(str(dampakberita), 'Dampak Berita Test')

    def test_tambahSubmisi_if(self):
        test = 'Anonymous'
        response_post = Client().post('/dampak/tambahSubmisi/', {'nama_berita': test, 'nama_penulis': test, 'isi_berita': test})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/dampak/lihatNonDetail/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
    
    def test_tambahSubmisi_else(self):
        test = 'Anonymous'
        response_post = Client().post('/dampak/tambahSubmisi/', {'nama_berita': '', 'nama_penulis': '', 'isi_berita': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/dampak/lihatNonDetail/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
    
    def test_POST_update(self):
        test = 'Anonymous'
        response = Client().post('/dampak/update_berita/1/', data= {'nama_berita': test, 'nama_penulis': test, 'isi_berita': test})
        jumlah = models.DampakBerita.objects.filter(nama_berita=test).count()
        self.assertEqual(jumlah,1)
    
    def test_POST_delete(self):
        test = 'Anonymous'
        response = Client().post('/dampak/delete_berita/1/', data= {'nama_berita': test, 'nama_penulis': test, 'isi_berita': test})
        jumlah = models.DampakBerita.objects.filter(nama_berita=test).count()
        self.assertEqual(jumlah,0)

    
    
    
    

     
    




    





        
    


