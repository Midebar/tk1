"""tugas5ppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
#from django.contrib import admin
from django.urls import path
#from django.contrib import admin

from . import views

urlpatterns = [
    #path('admin/', admin.site.urls),
    path('', views.webpage1, name='webpage1'),
    path('tambah/', views.webpage2, name='webpage2'),

    #path('hapus/', views.webpage3, name='webpage3'),
    path('lihat/', views.webpage4, name='webpage4'),
    path('lihatNonDetail/', views.webpage5, name='webpage5'),
    path('tambahSubmisi/', views.tambahSubmisi, name='tambahSubmisi'),
    path('update_berita/<str:pk>/', views.updateBerita, name='update_berita'),
    path('delete_berita/<str:pk>/', views.deleteBerita, name='delete_berita'),
    path('detail_berita/<str:pk>/', views.detailBerita, name='detail_berita'),



    #path('story1/', include('story1.urls')),
    #path('story3/', include('story3.urls')),
    #path('story3tambahan/', include('story3tambahan.urls')),
]
