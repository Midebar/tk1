from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from .models import *
from .forms import DampakBeritaForm
from django.shortcuts import render


def webpage1(request):
    return render(request, 'dampak/pageSelamatDatang.html')

def webpage2(request):
    return render(request, 'dampak/tambah.html')

def webpage4(request):
    dampaks = DampakBerita.objects.all()
    context = {'dampaks':dampaks}
    return render(request, 'dampak/lihat.html', context)

def webpage5(request):
    dampaks = DampakBerita.objects.all()
    context = {'dampaks':dampaks}
    return render(request, 'dampak/lihatNonDetail.html', context)

response = {}

def tambahSubmisi(request):
    form = DampakBeritaForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['nama_berita'] = request.POST['nama_berita']
        response['nama_penulis'] = request.POST['nama_penulis']
        response['isi_berita'] = request.POST['isi_berita']
        dampakberita = DampakBerita(nama_berita=response['nama_berita'],
            nama_penulis=response['nama_penulis'],isi_berita=response['isi_berita'])
        dampakberita.save()
        return redirect('/dampak/lihatNonDetail/')
    else:
        return redirect('/dampak/lihatNonDetail/')


def updateBerita(request, pk):
    dampak = DampakBerita.objects.get(pk=pk)
    form = DampakBeritaForm(instance=dampak)

    if (form.is_valid and request.method == 'POST'):
        form = DampakBeritaForm(request.POST, instance=dampak)
        form.save()
        return redirect('/dampak/lihatNonDetail/')

    context = {'form':form}
    return render(request, 'dampak/berita_update.html', context)

def deleteBerita(request, pk):
    dampak = DampakBerita.objects.get(id=pk)
    
    if request.method == "POST":
        dampak.delete()
        return redirect('/dampak/lihatNonDetail/')

    context = {'dampak':dampak}
    return render(request, 'dampak/berita_delete.html', context)


def detailBerita(request, pk):
    dampak = DampakBerita.objects.all().filter(id=pk)
    context = {'dampak':dampak}
    return render(request, 'dampak/lihatDetail.html', context)


